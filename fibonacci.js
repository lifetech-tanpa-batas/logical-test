const n = prompt("Masukkan jumlah deret *hanya angka*");
const array = [1, 1]; 

for (let i = 2; i < n; i++) {
    const fibonacci = array[i - 2] + array[i - 1];
    array.push(fibonacci);
}

const output = array.join(", ");
console.log(`Bilangan fibonacci [${output}]`);