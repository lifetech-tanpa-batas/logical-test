const outlets = [
    {
        id: 11,
        name: "Hik Jo"
    },
    {
        id: 12,
        name: "Outlet 1"
    }
];

const products = [
    {
        id: 1,
        outlet_id: 11,
        name: "Bakso Bakar"
    },
    {
        id: 2,
        outlet_id: 11,
        name: "Bakso Goreng"
    },
    {
        id: 3,
        outlet_id: 12,
        name: "Es Teh"
    }
];

for (const list of outlets) {
    list.Products = products.filter(items => items.outlet_id === list.id);
}

console.log(outlets);