const n = prompt("Masukkan jumlah baris *hanya angka*");
let result = "";

for (let i = n; i > 0; i--) {
    for (let j = 1; j <= n; j++) {
        if (j >= i) {
            result += "*";
        } else {
            result += " ";
        }
    }
    result += '\n';
}

console.log(result);